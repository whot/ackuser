# SPDX-Licences-Identifier: MIT

from ackuser import (
    not_read_only,
    Payload,
    InvalidPayload,
    WebhookIssueEvent,
    AckUserBuilder,
    AckUserIssue,
    BuilderError,
    USERBOT_USER_ID,
)
from typing import Optional
from unittest.mock import patch, MagicMock
from datetime import datetime, timedelta, timezone
from pathlib import Path

import attr
import json
import logging
import pytest
import subprocess
import yaml

logger = logging.getLogger("test-cli")


@pytest.fixture
def issue_event() -> dict:
    return json.load(open("tests/data/issue-event.json"))


@pytest.fixture
def push_event() -> dict:
    return json.load(open("tests/data/push-event.json"))


@pytest.fixture
def comment_event() -> dict:
    return json.load(open("tests/data/comment-event.json"))


@pytest.mark.parametrize("readonly", (True, False))
def test_if_not_readonly(caplog, readonly):
    caplog.set_level(logging.INFO, "ackuser")

    ro = not_read_only(readonly, "Some message")
    assert isinstance(ro, bool)
    assert ro != readonly
    assert "Some message" in caplog.text
    if readonly:
        assert "[RO SKIP]" in caplog.text
    else:
        assert "[RO SKIP]" not in caplog.text


def test_payload():
    data = json.loads(
        """
    {
        "foo": "bar",
        "ls": [1, 2, 3],
        "obj": {
            "baz": 10,
            "bat": 11
        },
        "foos": [
            { "foa": 20 },
            { "fob": 21 }
        ]
    }
    """
    )

    payload = Payload.from_json(data)
    assert payload.get("foo").value == "bar"
    assert payload.get("ls").values == [1, 2, 3]
    assert payload.get("obj").get("baz").value == 10
    assert payload.get("obj").get("bat").value == 11
    assert payload.get("obj", "baz").value == 10
    assert payload.get("obj", "bat").value == 11
    assert payload.get("obj").values == {"baz": 10, "bat": 11}
    assert payload.get("foos").values == [{"foa": 20}, {"fob": 21}]
    assert payload.get("foos").values == [{"foa": 20}, {"fob": 21}]

    with pytest.raises(InvalidPayload):
        payload.get("invalid")

    with pytest.raises(InvalidPayload):
        payload = Payload.from_json({})


def test_webhook_abstractions(issue_event: dict, push_event: dict, comment_event: dict):
    event = WebhookIssueEvent.from_json(issue_event)
    assert event.project.id == 1
    assert event.project.name == "Gitlab Test"
    assert event.project.namespace == "GitlabHQ"
    assert event.project.path_with_namespace == "gitlabhq/gitlab-test"
    assert event.project.web_url == "https://example.com/gitlabhq/gitlab-test"

    assert event.user.id == 1
    assert event.user.name == "Administrator"

    assert event.issue.id == 301
    assert event.issue.iid == 23
    assert event.issue.title == "New API: create/update/delete file"
    assert event.issue.author_id == 51

    assert event.added_labels[0].id == 205
    assert event.added_labels[0].title == "Account verification::approved"
    assert event.removed_labels[0].id == 206
    assert event.removed_labels[0].title == "API"

    with pytest.raises(InvalidPayload) as exc_info:
        WebhookIssueEvent.from_json(push_event)
    assert exc_info.type is InvalidPayload
    assert "does not look like an issue event" in exc_info.value.message

    with pytest.raises(InvalidPayload) as exc_info:
        WebhookIssueEvent.from_json(comment_event)
    assert exc_info.type is InvalidPayload
    assert "not permitted for issue events" in exc_info.value.message


@pytest.mark.parametrize("readonly", (True, False))
@patch("gitlab.Gitlab")
def test_builder(gitlab, readonly):
    @attr.s
    class Status:
        auth_called: bool = attr.ib(init=False, default=False)
        project: MagicMock = attr.ib(init=False, default=attr.Factory(MagicMock))  # type: ignore
        issue: MagicMock = attr.ib(init=False, default=attr.Factory(MagicMock))  # type: ignore

    status = Status()

    def auth_called():
        status.auth_called = True

    gitlab.auth = auth_called
    builder = AckUserBuilder.create_from_instance(gitlab)
    assert not status.auth_called, "Creating from instance must not call auth()"

    def projects_get(**kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == "test/foo"
        return status.project

    gitlab.projects = MagicMock()
    gitlab.projects.get = projects_get
    builder.set_project("test/foo")

    # Our API doesn't care if you call set_project twice, let's make use of that here

    def projects_get_id(**kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == 457
        return status.project

    gitlab.projects = MagicMock()
    gitlab.projects.get = projects_get_id
    builder.set_project_id(457)
    assert builder.project == status.project

    def issues_get(*args, **kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == 123
        return status.issue

    status.project.issues = MagicMock()
    status.project.issues.get = issues_get

    builder.set_issue_iid(123)
    assert builder.issue == status.issue

    builder.set_readonly(readonly)
    assert builder.readonly == readonly

    ackuser = builder.build()
    assert ackuser.gl == gitlab
    assert ackuser.project == status.project
    assert ackuser.issue == status.issue
    assert ackuser.readonly == readonly


@pytest.mark.parametrize("readonly", (True, False))
def test_approve(readonly):
    gl, project, issue, author = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )

    issue.id = 123
    issue.iid = 567
    issue.title = "some test issue"
    issue.assignee_ids = [1, 2, 3]
    issue.state = "open"
    issue.labels = ["existing-label"]
    project.name = "pytest run"
    author.id = "99"
    author.name = "test author"
    author.username = "atauthor"
    author.external = True
    gl.users.get.return_value = author

    @attr.s
    class Status:
        saved: bool = attr.ib(default=False)

    status = Status()

    def user_save(*args, **kwargs):
        assert not args
        assert not kwargs
        status.saved = True
        # Because we return the mocked author object from users.get,
        # author.external is modified directly

    author.save = user_save

    ackuser = AckUserIssue(gl, project, issue, readonly=readonly)
    ackuser.approve()

    # expect nothing to happen on the issue itself
    assert issue.labels == ["existing-label"]
    assert issue.assignee_ids == [1, 2, 3]
    assert issue.state == "open"

    if readonly:
        assert status.saved is False
        assert author.external is True
    else:
        assert status.saved is True
        assert author.external is False


@pytest.mark.parametrize("readonly", (True, False))
@pytest.mark.parametrize("close", (True, False))
@pytest.mark.parametrize("success", (None, True, False))
def test_update_issue(readonly, close, success):
    gl, project, issue = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )
    issue.labels = []

    ds_issue = AckUserIssue(gl=gl, project=project, issue=issue, readonly=readonly)
    ds_issue.update_with_message(message="foo", close=close, is_success=success)
    if readonly:
        issue.notes.create.assert_not_called()
        issue.notes.save.assert_not_called()
    else:
        issue.notes.create.assert_called_with({"body": "foo"})
        if close:
            assert issue.state_event == "close"
        if success is True:
            assert "ackuser::success" in issue.labels
        else:
            assert "ackuser::success" not in issue.labels

        if success is False:
            assert "ackuser::failed" in issue.labels
        else:
            assert "ackuser::failed" not in issue.labels

        if success is None:
            assert "ackuser::success" not in issue.labels
            assert "ackuser::failed" not in issue.labels

        issue.save.assert_called()
