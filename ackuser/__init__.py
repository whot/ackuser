# SPDX-Licences-Identifier: MIT

__version__ = "0.1.0"

from typing import Any, Optional, Union

import attr
import gitlab
import gitlab.v4.objects
import logging

logging.basicConfig(format="%(levelname)7s| %(name)s: %(message)s")
logger = logging.getLogger("ackuser")

USERBOT_USER_ID = 87895  # @userbot
FDO_PROJECT_ID = 1  # freedesktop/freedesktop

ACCOUNT_VERIFICATION_LABEL = "Account verification"
USER_APPROVED_LABEL = "Account verification::approved"


def not_read_only(ro: Union[Any, bool], message) -> bool:
    """
    Helper function for a read-only context that only executes the condition
    if readonly is `False`. Usage:

    >>> if not_read_only(self.readonly, "Deleting user"):
            user.delete()

    The user is only deleted if `self.readonly` is False.
    """
    if isinstance(ro, bool):
        readonly = ro
    else:
        assert hasattr(ro, "readonly")
        readonly = ro.readonly
    prefix = "[RO SKIP] " if readonly else ""
    logger.info(f"{prefix}{message}")
    return not readonly


@attr.s
class Payload:
    """
    Wrapper class to more conveniently access a json dictionary. In particular, this
    throws the InvalidPayload exception when trying to access a field that doesn't exist.

    This is not a complete wrapper, it has just enough functionality to let us
    parse the GitLab webhook payload for the fields we care bout.
    """

    payload: dict = attr.ib()

    @property
    def value(self):
        """
        Return the value of this item. If the value is a list or a dict, the value is
        dynamically wrapped into a new Payload so we can call `get()` and `value` on
        it again.

        If you need to access the list/dict items directly, use `values`
        """
        # Dynamically wrap any lists/dict values into new payload items
        if isinstance(self.payload, list):
            return [Payload(item) for item in self.payload]
        if isinstance(self.payload, dict):
            return {k: Payload(v) for k, v in self.payload}
        return self.payload

    @property
    def values(self):
        """
        Return the value of this item as-is if it is a list or a dict.
        """
        if isinstance(self.payload, list) or isinstance(self.payload, dict):
            return self.payload
        raise ValueError("Not an iterable value")

    def get(self, key, *keys) -> Any:
        """
        Get the data for the given key from the payload, stepping down into
        the extra keys if any are given::

        >>> import json
        >>> data = '{"foo": {"bar": "bat"}}'
        >>> payload = Payload.from_json(json.loads(data))
        >>> payload.get("foo").values
        {"bar", "bat"}
        >>> payload.get("foo", "bar").value
        bat

        """
        try:
            data = self.payload[key]
            for k in keys:
                data = data[k]
            return Payload(data)
        except KeyError as e:
            raise InvalidPayload(
                f"Missing key(s): '{e.args[0]}' ({'→'.join([key, *keys])}) in {self.payload}"
            )

    @classmethod
    def from_json(cls, json: dict) -> "Payload":
        if not json:
            raise InvalidPayload("Empty payloads are not allowed")
        return cls(payload=json)

    def __str__(self):
        return str(self.payload)


@attr.s
class InvalidPayload(Exception):
    """Thrown when trying to access a non-existing field in a payload"""

    message: str = attr.ib()


@attr.s
class WebhookGitlabUser:
    id: int = attr.ib()
    name: str = attr.ib()

    @classmethod
    def from_json(cls, payload: Payload) -> "WebhookGitlabUser":
        return cls(**{key: payload.get(key).value for key in ["id", "name"]})


@attr.s
class WebhookGitlabIssue:
    id: int = attr.ib()
    iid: int = attr.ib()
    title: str = attr.ib()
    author_id: int = attr.ib()

    @classmethod
    def from_json(cls, payload: Payload) -> "WebhookGitlabIssue":
        return cls(
            **{
                key: payload.get(key).value
                for key in ["id", "iid", "title", "author_id"]
            }
        )


@attr.s
class WebhookGitlabProject:
    id: int = attr.ib()
    name: str = attr.ib()
    namespace: str = attr.ib()
    path_with_namespace: str = attr.ib()
    web_url: str = attr.ib()

    @classmethod
    def from_json(cls, payload: Payload) -> "WebhookGitlabProject":
        return cls(
            **{
                key: payload.get(key).value
                for key in ["id", "name", "namespace", "path_with_namespace", "web_url"]
            }
        )


@attr.s
class WebhookGitlabLabel:
    id: int = attr.ib()
    title: str = attr.ib()

    @classmethod
    def from_json(cls, payload: Payload) -> "WebhookGitlabLabel":
        return cls(**{key: payload.get(key).value for key in ["id", "title"]})


@attr.s
class WebhookIssueEvent:
    project: WebhookGitlabProject = attr.ib()
    user: WebhookGitlabUser = attr.ib()
    issue: WebhookGitlabIssue = attr.ib()
    labels: list[WebhookGitlabLabel] = attr.ib()
    added_labels: list[WebhookGitlabLabel] = attr.ib()
    removed_labels: list[WebhookGitlabLabel] = attr.ib()

    @classmethod
    def from_json(cls, json: dict) -> "WebhookIssueEvent":
        payload = Payload.from_json(json)

        # some webhooks have event_type, others have event_name. yay
        try:
            event_type = payload.get("event_type").value
        except InvalidPayload:
            raise InvalidPayload("payload does not look like an issue event")
        else:
            if event_type not in ["issue", "confidential_issue"]:
                raise InvalidPayload(
                    f"event_type '{event_type}' is not permitted for issue events"
                )

        project = WebhookGitlabProject.from_json(payload.get("project"))
        user = WebhookGitlabUser.from_json(payload.get("user"))
        issue = WebhookGitlabIssue.from_json(payload.get("object_attributes"))
        labels = [WebhookGitlabLabel.from_json(l) for l in payload.get("labels").value]
        # Webhook test payloads have an empty "changes" field - if that
        # happens use the labels from the issue and treat every label
        # as newly added
        try:
            payload.get("changes", "labels")
        except InvalidPayload:
            logger.warning("No changed labels in data, using current label set instead")
            labels_before = []
            labels_current = labels
        else:
            labels_before = [
                WebhookGitlabLabel.from_json(l)
                for l in payload.get("changes", "labels", "previous").value
            ]
            labels_current = [
                WebhookGitlabLabel.from_json(l)
                for l in payload.get("changes", "labels", "current").value
            ]

        removed_labels = list(filter(lambda e: e not in labels_current, labels_before))
        added_labels = list(filter(lambda e: e not in labels_before, labels_current))

        return cls(
            project=project,
            user=user,
            issue=issue,
            labels=labels,
            added_labels=added_labels,
            removed_labels=removed_labels,
        )


@attr.s
class BuilderError(Exception):
    message: str = attr.ib()

    def __str__(self):
        return f"Builder Error: {self.message}"


@attr.s
class AckUserBuilder:
    gl: gitlab.Gitlab = attr.ib()  # type: ignore
    project: Optional[gitlab.v4.objects.Project] = attr.ib(default=None)
    issue: Optional[gitlab.v4.objects.ProjectIssue] = attr.ib(init=False, default=None)
    user: Optional[gitlab.v4.objects.User] = attr.ib(init=False, default=None)
    # We default to readonly true so a runaway bot can't change anything and any copies of
    # this object explicitly needs to disable readonly
    readonly: bool = attr.ib(init=False, default=True)

    @classmethod
    def create_from_url(cls, url: str, private_token: str) -> "AckUserBuilder":
        logger.debug(f"Using GitLab issue: {url}")
        gl = gitlab.Gitlab(url=url, private_token=private_token)  # type: ignore
        gl.auth()
        return cls(gl=gl)

    @classmethod
    def create_from_instance(
        cls,
        gl: gitlab.Gitlab,  # type: ignore
    ) -> "AckUserBuilder":
        return cls(gl=gl)

    def set_project(self, full_name: str) -> "AckUserBuilder":
        self.project = self.gl.projects.get(id=full_name)
        if self.project is None:
            raise BuilderError(f"Project {full_name} not found")
        logger.debug(f"Using GitLab project: {full_name}")
        return self

    def set_project_id(self, id: int) -> "AckUserBuilder":
        self.project = self.gl.projects.get(id=id)
        if self.project is None:
            raise BuilderError("Project {id} not found")
        logger.debug(f"Using GitLab project: {self.project.path_with_namespace}")
        return self

    def set_issue_iid(self, issue_iid: int) -> "AckUserBuilder":
        logger.debug(f"Using GitLab project issue: #{issue_iid}")
        if self.project is None:
            raise BuilderError("Cannot fetch an issue without a project")
        issue = self.project.issues.get(id=issue_iid)
        if issue is None:
            raise BuilderError("Project issue {issue_iid} not found")
        self.issue = issue
        return self

    def set_readonly(self, readonly: bool) -> "AckUserBuilder":
        self.readonly = readonly
        return self

    def build(self) -> "AckUserIssue":
        if any([x is None for x in (self.project, self.issue, self.gl)]):
            raise BuilderError("Cannot build without all fields set")

        # to shut up mypy:
        assert self.gl is not None
        assert self.project is not None
        assert self.issue is not None
        return AckUserIssue(
            gl=self.gl,
            project=self.project,
            issue=self.issue,
            readonly=self.readonly,
        )


@attr.s
class AckUserIssue:
    gl: gitlab.Gitlab = attr.ib()  # type: ignore
    project: gitlab.v4.objects.Project = attr.ib()
    issue: gitlab.v4.objects.ProjectIssue = attr.ib()
    readonly: bool = attr.ib(default=False)

    def approve(self) -> bool:
        author = self.gl.users.get(id=self.issue.author["id"])
        if not author.external:
            logger.debug(f"User {author.name} is not external, we're done")
            return True

        if not_read_only(
            self, f"Setting user {author.name} (@{author.username}) to internal"
        ):
            author.external = False
            author.save()

        return True

    def update_with_message(
        self, message: str, close: bool, is_success: Optional[bool] = None
    ):
        """
        Update the issue with the message and optionally closing it.
        This also assigns the ackuser label and if is_succcess is True
        or False (but not None), the ackuser::success/failed label.
        """
        if not_read_only(self, message):
            self.issue.assignee_ids = [USERBOT_USER_ID]
            labels = self.issue.labels + [ACCOUNT_VERIFICATION_LABEL]
            labels += {
                True: ["ackuser::success"],
                False: ["ackuser::failed"],
                None: [],
            }[is_success]
            self.issue.labels = labels
            self.issue.notes.create({"body": message})
            if close:
                self.issue.state_event = "close"
            self.issue.save()
