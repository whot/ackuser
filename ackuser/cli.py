# SPDX-Licences-Identifier: MIT
#
# This file is formatted with Python Black

from typing import Optional, Tuple
from pathlib import Path

import attr
import click
import datetime
import logging
import json
import os
import re

import ackuser as ack

logger = logging.getLogger("ackuser")

DEFAULT_INSTANCE = "https://gitlab.freedesktop.org"


def xdg_config_home() -> Path:
    return Path(os.environ.get("XDG_CONFIG_HOME", Path.home() / ".config"))


def xdg_token(name: str = "userbot.token") -> Optional[str]:
    xdg_path = xdg_config_home() / "ackuser" / name
    try:
        token = open(xdg_path).read().rstrip()
    except FileNotFoundError:
        return None
    return token


def die(message: str):
    click.echo(message)
    raise SystemExit(1)


@attr.s
class Project:
    instance: str = attr.ib()
    path_with_namespace: str = attr.ib()

    @classmethod
    def from_url(
        cls,
        url: str,
    ) -> Optional["Project"]:
        matches = re.match(
            rf"(?P<instance>https://[^/]+)/(?P<project>[\w_/-]+)/?",
            url,
        )
        if not matches:
            return None

        return cls(instance=matches["instance"], path_with_namespace=matches["project"])


@attr.s
class Issue:
    project: Project = attr.ib()
    iid: int = attr.ib()

    @classmethod
    def from_url(cls, url: str) -> Optional["Issue"]:
        match = re.match(
            rf"(?P<instance>https://[^/]+)/(?P<project>[\w_/-]+)/-/issues/(?P<issue>[0-9]+)",
            url,
        )
        try:
            instance, project, issue_iid = match["instance"], match["project"], int(match["issue"])  # type: ignore
            return Issue(
                project=Project(instance=instance, path_with_namespace=project),
                iid=issue_iid,
            )
        except (TypeError, IndexError, ValueError):
            return None


@attr.s
class Config:
    verbose: bool = attr.ib()
    readonly: bool = attr.ib()
    token: Optional[str] = attr.ib()


@click.group()
@click.option("--verbose", "-v", is_flag=True, help="Enable debug logging")
@click.option("--readonly", is_flag=True, help="Print, but don't modify")
@click.option(
    "--token-file",
    type=click.Path(exists=True, dir_okay=False),
    help="File containing the GitLab API token. Defaults to XDG_CONFIG_HOME/ackuser/userbot.token",
)
@click.option(
    "--token-env",
    type=str,
    help="Environment variable containing the GitLab API token",
)
@click.pass_context
def ackuser(
    ctx,
    verbose: bool,
    readonly: bool,
    token_file: Optional[click.Path],
    token_env: Optional[str],
):
    global logger
    """
    Approve a user, i.e. untick the "external" checkbox.

    This tool is intended to be passed a GitLab issue-event webhook payload, see
    'ackuser approve-hook --help'. It can be run manually with the 'approve' command.
    The functionality is largely identical.
    """
    logger.setLevel(logging.DEBUG if verbose else logging.INFO)

    if token_file is not None:
        token = open(token_file).read().rstrip() or die("Empty token file")  # type: ignore
    elif token_env is not None:
        token = os.environ.get(token_env) or die(
            "Requested token environment variable does not exist, use --token-env"
        )
    else:
        token = None

    ctx.obj = Config(verbose=verbose, readonly=readonly, token=token)


@ackuser.command(name="approve-hook", help="Process a GitLab issue event webhook")
@click.option(
    "--payload-file",
    type=click.Path(exists=True, dir_okay=False),
    help="File containing the payload",
)
@click.option(
    "--payload-env", type=str, help="Environment variable containing the payload"
)
@click.pass_context
def do_approve_hook(
    ctx,
    payload_file: Optional[click.Path],
    payload_env: Optional[str],
):
    """
    Process the payload from a Gitlab issue-event web hook. This hook checks for
    whether the "Account verification::approved" label has been added and if so,
    it looks up the user that filed this issue and marks the user as no longer external.
    """
    if payload_file is not None:
        payload_string = open(payload_file).read()  # type: ignore
    elif payload_env is not None:
        payload_string = os.environ.get(payload_env) or die(
            "Requested payload environment variable does not exist, use --payload-from"
        )
    else:
        die("One of --payload-file or --payload-env is required")

    try:
        js = json.loads(payload_string)
        event = ack.WebhookIssueEvent.from_json(js)
        logger.debug(
            f"Webhook issue event for {event.project.path_with_namespace}#{event.issue.iid}"
        )
    except json.JSONDecodeError as e:
        die(f"JSON parser failed: {e}")
    except ack.InvalidPayload as e:
        die(f"Invalid data: {e}")

    # We have the webhook data parsed as event now, let's do our thing
    if event.user.id == ack.USERBOT_USER_ID:
        logger.debug("Ignoring events triggered by userbot")
        raise SystemExit(0)

    label = ack.USER_APPROVED_LABEL
    if label not in [l.title for l in event.added_labels]:
        logger.debug(f"No new label {label}")
        raise SystemExit(0)

    # project URL includes the project path
    instance_url = event.project.web_url.removesuffix(
        event.project.path_with_namespace
    ).rstrip("/")

    config = ctx.obj
    token = (
        config.token
        or xdg_token()
        or die("One of --token-file or --token-env is required")
    )

    builder = (
        ack.AckUserBuilder.create_from_url(instance_url, token)
        .set_project_id(event.project.id)
        .set_issue_iid(event.issue.iid)
        .set_readonly(config.readonly)
    )

    ackuser = builder.build()
    success = ackuser.approve()
    if success:
        message = "All done. Welcome and thanks for your future contributions! :tada:"
    else:
        message = "I was asked to look at this issue, but something went wrong. I'm just a simple bot. Please approve manually."

    ackuser.update_with_message(message, close=success, is_success=success)


def check_url(ctx, param, value):
    if Issue.from_url(value) is None:
        raise click.BadParameter(f"Invalid GitLab issue URL ")
    return value


@ackuser.command(
    name="approve", help="Process the given GitLab account verification issue"
)
@click.argument(
    "issue",
    type=str,
    callback=check_url,
)
@click.pass_context
def do_approve(
    ctx,
    issue: str,
):
    """
    Approve the user that filed the issue, applying the Account verification::aproved label in the process.

    The issue is 123 or https://gitlab.freedesktop.org/freedesktop/freedesktop/-/issues/123
    The host name must match the --instance argument.
    """
    config = ctx.obj
    iss = Issue.from_url(issue)
    if not iss:
        die(f"Failed to parse issue {issue}")

    token = (
        config.token
        or xdg_token()
        or die("One of --token-file or --token-env is required")
    )
    builder = (
        ack.AckUserBuilder.create_from_url(iss.project.instance, token)
        .set_project(iss.project.path_with_namespace)
        .set_issue_iid(iss.iid)
        .set_readonly(config.readonly)
    )

    ackuser = builder.build()
    success = ackuser.approve()
    if success:
        message = "All done. Welcome and thanks for your future contributions! :tada:"
    else:
        message = "I was asked to look at this issue, but something went wrong. I'm just a simple bot. Please approve manually."

    ackuser.update_with_message(message, close=success, is_success=success)


if __name__ == "__main__":
    ackuser()
